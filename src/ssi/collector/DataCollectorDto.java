package ssi.collector;

import javafx.beans.property.ObjectProperty;
import javafx.scene.control.Slider;
import javafx.scene.image.ImageView;

public class DataCollectorDto {

    private ImageView imageView_depthMap;
    private ImageView imageView_segmentedImage;
    private ImageView imageView_vhist;
    private ImageView imageView_uhist;
    private ImageView imageView_ground;

    private ObjectProperty<String> distanceValuesProp;
    private ObjectProperty<String> deviceInfoValuesProp;

    private Slider slider_vHistThreshold;
    private Slider slider_uHistThreshold;
    private Slider slider_minComponetSize;

    private Slider slider_erodeKernelSize;
    private Slider slider_dilateKernelSize;

    public ImageView getImageView_depthMap() {
        return imageView_depthMap;
    }

    public void setImageView_depthMap(ImageView imageView_depthMap) {
        this.imageView_depthMap = imageView_depthMap;
    }

    public ImageView getImageView_segmentedImage() {
        return imageView_segmentedImage;
    }

    public void setImageView_segmentedImage(ImageView imageView_segmentedImage) {
        this.imageView_segmentedImage = imageView_segmentedImage;
    }

    public ImageView getImageView_vhist() {
        return imageView_vhist;
    }

    public void setImageView_vhist(ImageView imageView_vhist) {
        this.imageView_vhist = imageView_vhist;
    }

    public ImageView getImageView_uhist() {
        return imageView_uhist;
    }

    public void setImageView_uhist(ImageView imageView_uhist) {
        this.imageView_uhist = imageView_uhist;
    }

    public ImageView getImageView_ground() {
        return imageView_ground;
    }

    public void setImageView_ground(ImageView imageView_ground) {
        this.imageView_ground = imageView_ground;
    }

    public ObjectProperty<String> getDistanceValuesProp() {
        return distanceValuesProp;
    }

    public void setDistanceValuesProp(ObjectProperty<String> distanceValuesProp) {
        this.distanceValuesProp = distanceValuesProp;
    }

    public ObjectProperty<String> getDeviceInfoValuesProp() {
        return deviceInfoValuesProp;
    }

    public void setDeviceInfoValuesProp(ObjectProperty<String> deviceInfoValuesProp) {
        this.deviceInfoValuesProp = deviceInfoValuesProp;
    }

    public Slider getSlider_vHistThreshold() {
        return slider_vHistThreshold;
    }

    public void setSlider_vHistThreshold(Slider slider_vHistThreshold) {
        this.slider_vHistThreshold = slider_vHistThreshold;
    }

    public Slider getSlider_uHistThreshold() {
        return slider_uHistThreshold;
    }

    public void setSlider_uHistThreshold(Slider slider_uHistThreshold) {
        this.slider_uHistThreshold = slider_uHistThreshold;
    }

    public Slider getSlider_minComponetSize() {
        return slider_minComponetSize;
    }

    public void setSlider_minComponetSize(Slider slider_minComponetSize) {
        this.slider_minComponetSize = slider_minComponetSize;
    }

    public Slider getSlider_erodeKernelSize() {
        return slider_erodeKernelSize;
    }

    public void setSlider_erodeKernelSize(Slider slider_erodeKernelSize) {
        this.slider_erodeKernelSize = slider_erodeKernelSize;
    }

    public Slider getSlider_dilateKernelSize() {
        return slider_dilateKernelSize;
    }

    public void setSlider_dilateKernelSize(Slider slider_dilateKernelSize) {
        this.slider_dilateKernelSize = slider_dilateKernelSize;
    }
}
