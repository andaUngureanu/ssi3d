package ssi.collector;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public final class Utils {
    public static Image mat2Image(Mat frame) {
        try {
            return SwingFXUtils.toFXImage(matToBufferedImage(frame), null);
        } catch (Exception exception) {
            System.err.println("Cannot convert the Mat object: " + exception);
            return null;
        }
    }

    public static <T> void onFXThread(final ObjectProperty<T> property, final T value) {
        Platform.runLater(() -> {
            property.set(value);
        });
    }

    public static void updateImageView(ImageView view, Image image) {
        Utils.onFXThread(view.imageProperty(), image);

    }

    public static Image bufferedImageToImage(BufferedImage img) {
        try {
            return SwingFXUtils.toFXImage(img, null);
        } catch (Exception exception) {
            System.err.println("Cannot convert the Mat object: " + exception);
            return null;
        }
    }

    private static BufferedImage matToBufferedImage(Mat original) {
        BufferedImage image = null;
        int width = original.width(), height = original.height(), channels = original.channels();
        if (original.type() != CvType.CV_8U) {
            int[] sourcePixels = new int[width * height * channels];
            original.get(0, 0, sourcePixels);
            image = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
            final int[] targetPixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
            System.arraycopy(sourcePixels, 0, targetPixels, 0, sourcePixels.length);
        } else {
            byte[] sourcePixels = new byte[width * height * channels];
            original.get(0, 0, sourcePixels);
            image = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
            final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
            System.arraycopy(sourcePixels, 0, targetPixels, 0, sourcePixels.length);
        }
        return image;
    }
}