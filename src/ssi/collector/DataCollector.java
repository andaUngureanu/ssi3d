package ssi.collector;

import org.openni.*;
import ssi.connector.DeviceContainer;
import ssi.processor.DataProcessor;
import ssi.processor.FrameProcessorDto;

public class DataCollector implements VideoStream.NewFrameListener {
    private DeviceContainer deviceContainer;
    private DataProcessor dataProcessor;
    private DataCollectorDto dataCollectorDto;

    public DataCollector(DataCollectorDto dataCollectorDto) {
        this.dataCollectorDto = dataCollectorDto;
        dataProcessor = new DataProcessor(dataCollectorDto);
    }

    public void initStream(String url) {
        deviceContainer = new DeviceContainer(url);
        deviceContainer.getDepthStream().addNewFrameListener(this);
        Utils.onFXThread(dataCollectorDto.getDeviceInfoValuesProp(), deviceContainer.printDeviceInfo());
    }

    public void pauseStream() {
        deviceContainer.getDepthStream().stop();
    }

    public void startStream() {
        deviceContainer.getDepthStream().start();
    }

    public DeviceContainer getDeviceContainer() {
        return deviceContainer;
    }

    @Override
    public void onFrameReady(VideoStream videoStream) {
        FrameProcessorDto frameProcessorDto = dataProcessor.processFrame(videoStream);

        Utils.onFXThread(dataCollectorDto.getDistanceValuesProp(), frameProcessorDto.getDistanceValuesProp());
        Utils.updateImageView(dataCollectorDto.getImageView_segmentedImage(), frameProcessorDto.getImage_segmentedImage());
        Utils.updateImageView(dataCollectorDto.getImageView_uhist(), frameProcessorDto.getImage_uhist());
        Utils.updateImageView(dataCollectorDto.getImageView_vhist(), frameProcessorDto.getImage_vhist());
        Utils.updateImageView(dataCollectorDto.getImageView_depthMap(), frameProcessorDto.getImage_depthMap());
        Utils.updateImageView(dataCollectorDto.getImageView_ground(), frameProcessorDto.getImage_ground());
    }
}
