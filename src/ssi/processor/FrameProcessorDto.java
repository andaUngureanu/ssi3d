package ssi.processor;

import javafx.scene.image.Image;

public class FrameProcessorDto {

    private Image image_depthMap;
    private Image image_segmentedImage;
    private Image image_vhist;
    private Image image_uhist;
    private Image image_ground;

    private String distanceValuesProp;

    public Image getImage_depthMap() {
        return image_depthMap;
    }

    public void setImage_depthMap(Image image_depthMap) {
        this.image_depthMap = image_depthMap;
    }

    public Image getImage_segmentedImage() {
        return image_segmentedImage;
    }

    public void setImage_segmentedImage(Image image_segmentedImage) {
        this.image_segmentedImage = image_segmentedImage;
    }

    public Image getImage_vhist() {
        return image_vhist;
    }

    public void setImage_vhist(Image image_vhist) {
        this.image_vhist = image_vhist;
    }

    public Image getImage_uhist() {
        return image_uhist;
    }

    public void setImage_uhist(Image image_uhist) {
        this.image_uhist = image_uhist;
    }

    public Image getImage_ground() {
        return image_ground;
    }

    public void setImage_ground(Image image_ground) {
        this.image_ground = image_ground;
    }

    public String getDistanceValuesProp() {
        return distanceValuesProp;
    }

    public void setDistanceValuesProp(String distanceValuesProp) {
        this.distanceValuesProp = distanceValuesProp;
    }
}
