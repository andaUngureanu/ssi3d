package ssi.processor;

import javafx.scene.image.Image;
import javafx.scene.image.PixelFormat;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.openni.VideoFrameRef;
import org.openni.VideoStream;
import ssi.collector.DataCollectorDto;
import ssi.collector.Utils;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.*;

import static ssi.processor.Constants.*;

public class DataProcessor {
    private FrameProcessorDto frameProcessorDto;
    private DataCollectorDto dataCollectorDto;

    private float[] histogram;
    private int[] imagePixels;
    private int[] depthPixels;
    private int[] vHistLines;

    private int width;
    private int height;

    private int maxPixelValue;

    private VideoFrameRef lastFrame;
    private BufferedImage bufferedImage;
    private Vector<Vector<Point>> connectedComponents;
    private Map<Integer, Rect> labelToRect;

    public DataProcessor(DataCollectorDto dataCollectorDto) {
        this.frameProcessorDto = new FrameProcessorDto();
        this.dataCollectorDto = dataCollectorDto;
        this.connectedComponents = new Vector<>();
        this.labelToRect = new HashMap<>();
    }


    public void draw() {
        if (lastFrame == null) {
            return;
        }

        drawVHistogram();

        int[] segArray = detectGround(width, height);

        drawUHistogram();

        String distances = detectObstacles(width, height, segArray);

        BufferedImage segBuff = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
        segBuff.setRGB(0, 0, width, height, segArray, 0, width);

        Image segImg = Utils.bufferedImageToImage(segBuff);
        frameProcessorDto.setImage_segmentedImage(segImg);
        frameProcessorDto.setDistanceValuesProp(distances);
    }

    private String detectObstacles(int width, int height, int[] segArray) {
        int minDistanceLeft = maxPixelValue + 10;
        int minDistanceCenter = maxPixelValue + 10;
        int minDistanceRight = maxPixelValue + 10;

        for (int i = 0; i < connectedComponents.size(); ++i) {
            char r = (char) (i * 100 + 200);
            char g = (char) (i * 100 + 300);
            char b = (char) (i * 100 + 400);

            for (int j = 0; j < connectedComponents.get(i).size(); ++j) {
                int x = (int) connectedComponents.get(i).get(j).x;
                int y = (int) connectedComponents.get(i).get(j).y;
                for (int yImg = 0; yImg < height; ++yImg) {
                    if (depthPixels[yImg * width + x] != 0 && depthPixels[yImg * width + x] / U_HISTO_SCALE == y) { //
                        segArray[yImg * width + x] = b | g << 8 | r << 16;
                        if (x <= width / 3 && minDistanceLeft > y * U_HISTO_SCALE) {
                            minDistanceLeft = y * U_HISTO_SCALE;
                        }
                        if (width / 3 < x && x <= 2 * width / 3 && minDistanceCenter > y * U_HISTO_SCALE) {
                            minDistanceCenter = y * U_HISTO_SCALE;
                        }
                        if (2 * width / 3 < x && x <= width && minDistanceRight > y * U_HISTO_SCALE) {
                            minDistanceRight = y * U_HISTO_SCALE;
                        }
                    }
                }

            }

        }
        return printDistance(minDistanceLeft, minDistanceCenter, minDistanceRight);
    }

    private int[] detectGround(int width, int height) {
        List<Double> pointsInGround = new ArrayList<>();
        List<Double> zCoordsOfPoints = new ArrayList<>();
        int vWidth = MAX_DISTANCE / V_HISTO_SCALE;
        int vHeight = height;
        for (int i = 0; i < vHeight; ++i) {
            for (int j = 0; j < vWidth; ++j) {
                if (vHistLines[i * vWidth + j] == -65536) {

                    for (int k = 0; k < width; ++k) {
                        if (depthPixels[i * width + k] / V_HISTO_SCALE == j) {
                            int y = i * width;
                            int x = k;
                            pointsInGround.add((double) x);
                            pointsInGround.add((double) y);
                            pointsInGround.add((double) 1);
                            zCoordsOfPoints.add((double) depthPixels[i * width + k]);
                        }
                    }
                }
            }
        }

        int[] segArray = new int[width * height];
        for (int i = 0; i < segArray.length; ++i) {
            segArray[i] = 0;
        }
        if (pointsInGround.size() > 3) {
            int nrPoints = pointsInGround.size() / 3;

            Mat A = new Mat(nrPoints, 3, CvType.CV_64FC1);
            Mat X = new Mat(3, 1, CvType.CV_64FC1);
            Mat Z = new Mat(nrPoints, 1, CvType.CV_64FC1);
            double[] points = new double[pointsInGround.size()];
            for (int i = 0; i < pointsInGround.size(); ++i) {
                points[i] = pointsInGround.get(i);
            }
            double[] zCoords = new double[nrPoints];
            for (int i = 0; i < nrPoints; ++i) {
                zCoords[i] = zCoordsOfPoints.get(i);
            }
            A.put(0, 0, points);
            Z.put(0, 0, zCoords);

            Core.solve(A, Z, X, Core.DECOMP_SVD);
            double[] planeEc = new double[3];
            X.get(0, 0, planeEc);

            for (int i = 0; i < vHeight; ++i) {
                for (int j = 0; j < width; ++j) {
                    int y = i * width;
                    int x = j;

                    if (x * planeEc[0] + y * planeEc[1] + planeEc[2] >= depthPixels[x + y] - 350
                            && x * planeEc[0] + y * planeEc[1] + planeEc[2] <= depthPixels[x + y] + 350) {

                        segArray[x + y] = GROUND_PIXEL_VAL;
                        depthPixels[x + y] = 0;
                    }

                }
            }
        }
        return segArray;
    }

    private String printDistance(int minDistanceLeft, int minDistanceCenter, int minDistanceRight) {
        StringBuilder distances = new StringBuilder().append("Number of obstacles: ").append(connectedComponents.size()).append("\n");

        distances.append("Closest left obstacle: ");
        if (minDistanceLeft < maxPixelValue) {
            distances.append(minDistanceLeft * 1.0 / 10).append(" cm\n");
        } else {
            distances.append("inf\n");
        }

        distances.append("Closest center obstacle: ");
        if (minDistanceCenter < maxPixelValue) {
            distances.append(minDistanceCenter * 1.0 / 10).append(" cm\n");
        } else {
            distances.append("inf\n");
        }

        distances.append("Closest right obstacle: ");
        if (minDistanceRight < maxPixelValue) {
            distances.append(minDistanceRight * 1.0 / 10).append(" cm");
        } else {
            distances.append("inf");
        }

        return distances.toString();
    }

    public void drawVHistogram() {

        int vWidth = MAX_DISTANCE / V_HISTO_SCALE;
        int vHeight = height;
        byte byteArray[] = new byte[vHeight * vWidth];
        int[] vHistogram = new int[vWidth * vHeight];

        for (int i = 0; i < vHistogram.length; ++i) {
            vHistogram[i] = 0;
            byteArray[i] = 0;
        }

        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                if (depthPixels[i * width + j] > 0
                        && depthPixels[i * width + j] < MAX_DISTANCE) {
                    vHistogram[i * vWidth + (depthPixels[i * width + j]) / V_HISTO_SCALE]++;

                }
            }
        }

        for (int i = 0; i < vHistogram.length; ++i) {
            if (vHistogram[i] < dataCollectorDto.getSlider_vHistThreshold().getValue()) {
                vHistogram[i] = 0;
            } else {
                vHistogram[i] = 255 << 8;
                byteArray[i] = ~0;
            }
        }

        Mat image = new Mat(vHeight, vWidth, CvType.CV_8U);
        image.put(0, 0, byteArray);

        Mat lines = new Mat();

        double rhoInit = 1, thetaInit = Math.PI / 180;
        Imgproc.HoughLinesP(image, lines, rhoInit, thetaInit, 5, 5, 10);

        Mat vOutput = new Mat(vHeight, vWidth, CvType.CV_32S);
        vOutput.put(0, 0, vHistogram);
        for (int x = 0; x < lines.rows(); x++) {
            double[] l = lines.get(x, 0);
            double x0 = l[0], y0 = l[1], x1 = l[2], y1 = l[3];
            double angle = -Math.atan((y1 - y0) / (x1 - x0));
            if (0 < angle && angle < Math.PI / 2) {
                Imgproc.line(vOutput, new Point(x0, y0), new Point(x1, y1), new Scalar(255), 1, Imgproc.LINE_AA, 0);

            }
        }
        Image outputImage = Utils.mat2Image(vOutput);
        vHistLines = new int[vHeight * vWidth];

        outputImage.getPixelReader().getPixels(0, 0, vWidth, vHeight, PixelFormat.getIntArgbInstance(), vHistLines, 0,
                vWidth);

        frameProcessorDto.setImage_vhist(outputImage);
    }

    public void drawUHistogram() {
        int uHeight = MAX_DISTANCE / U_HISTO_SCALE;
        int uWidth = width;
        int[] uHistogram = new int[uWidth * uHeight];

        for (int i = 0; i < uHistogram.length; ++i)
            uHistogram[i] = 0;

        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                if (depthPixels[i * width + j] > 0
                        && depthPixels[i * width + j] <= MAX_DISTANCE) {
                    uHistogram[(depthPixels[i * width + j]) / U_HISTO_SCALE * uWidth + j]++;
                }
            }
        }

        for (int i = 0; i < uHistogram.length; ++i) {
            if (uHistogram[i] < dataCollectorDto.getSlider_uHistThreshold().getValue()) {
                uHistogram[i] = 0;
            } else {
                uHistogram[i] = ~0;
            }
        }

        Mat image = new Mat(uHeight, uWidth, CvType.CV_32SC1);
        image.put(0, 0, uHistogram);
        image.convertTo(image, CvType.CV_8UC1, -255, 0);
        int erodeKernelSize = (int) dataCollectorDto.getSlider_erodeKernelSize().getValue();
        int dilateKernelSize = (int) dataCollectorDto.getSlider_dilateKernelSize().getValue();
        Mat erodeKernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,
                new Size(2 * erodeKernelSize + 1, 2 * erodeKernelSize + 1));
        Mat dilateKernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,
                new Size(2 * dilateKernelSize + 1, 2 * dilateKernelSize + 1));
        Imgproc.dilate(image, image, dilateKernel);
        Imgproc.erode(image, image, erodeKernel);
        image.convertTo(image, CvType.CV_32SC1, (Math.pow(2, 24) - 1) / 255);
        int labelCount = 6000;

        for (int y = 0; y < image.rows(); y++) {
            int[] row = new int[image.cols()];
            image.get(y, 0, row);
            for (int x = 0; x < image.cols(); ++x) {
                if (row[x] == 0 || row[x] != 16777215) {
                    continue;
                }
                Mat mask = Mat.zeros(image.rows() + 2, image.cols() + 2, CvType.CV_8U);
                Rect rect = new Rect();
                Imgproc.floodFill(image, mask, new Point(x, y), new Scalar(labelCount), rect, new Scalar(0),
                        new Scalar(0), 8);

                labelToRect.put(labelCount, rect);
                labelCount++;
                image.get(y, 0, row);
            }

        }
        for (Map.Entry<Integer, Rect> entry : labelToRect.entrySet()) {
            Integer label = entry.getKey();
            Rect rect = entry.getValue();
            Vector<Point> component = new Vector<Point>();
            for (int i = rect.y; i < rect.y + rect.height; i++) {
                int[] row2 = new int[rect.x + rect.width];
                image.get(i, 0, row2);
                for (int j = rect.x; j < (rect.x + rect.width); ++j) {
                    if (row2[j] != label) {
                        continue;
                    }
                    component.add(new Point(j, i));
                }
            }
            if (component.size() > dataCollectorDto.getSlider_minComponetSize().getValue()) {
                connectedComponents.add(component);
            } else {
                Mat mask = Mat.zeros(image.rows() + 2, image.cols() + 2, CvType.CV_8U);
                Rect rect1 = new Rect();
                Imgproc.floodFill(image, mask, component.get(0), new Scalar(0), rect1, new Scalar(0),
                        new Scalar(0), 8);
            }
        }
        for (int i = 0; i < connectedComponents.size(); ++i) {
            char r = (char) (i * 100 + 200);
            char g = (char) (i * 100 + 300);
            char b = (char) (i * 100 + 400);
            for (int j = 0; j < connectedComponents.get(i).size(); ++j) {
                int x = (int) connectedComponents.get(i).get(j).x;
                int y = (int) connectedComponents.get(i).get(j).y;
                image.put(y, x, r | g << 8 | b << 16);
            }
        }
        int x1 = uWidth / 3;
        int x2 = 2 * uWidth / 3;
        Imgproc.line(image, new Point(x1, 0), new Point(x1, uHeight), new Scalar(255), 1, Imgproc.LINE_AA, 0);
        Imgproc.line(image, new Point(x2, 0), new Point(x2, uHeight), new Scalar(255), 1, Imgproc.LINE_AA, 0);

        Image uhistImage = Utils.mat2Image(image);
        frameProcessorDto.setImage_uhist(uhistImage);
    }

    public void calculateHistogram(ByteBuffer data) {
        for (int i = 0; i < histogram.length; ++i)
            histogram[i] = 0;

        int points = 0;
        while (data.remaining() > 0) {
            int pixel = data.getShort();
            if (pixel != 0) {
                histogram[pixel]++;
                points++;
            }
        }
        for (int i = 1; i < histogram.length; i++) {
            histogram[i] += histogram[i - 1];
        }

        if (points > 0) {
            for (int i = 1; i < histogram.length; i++) {
                histogram[i] = (int) (65536 * (1.0f - (histogram[i] / points)));
            }

        }
    }

    public FrameProcessorDto processFrame(VideoStream videoStream) {
        init(videoStream);

        ByteBuffer frameData = videoStream.readFrame().getData().order(ByteOrder.LITTLE_ENDIAN);
        calculateHistogram(frameData);
        frameData.rewind();

        int pos = 0;
        while (frameData.remaining() > 0) {
            int depth = frameData.getShort();
            int pixel = (int) histogram[depth];
            depthPixels[pos] = depth;
            imagePixels[pos] = pixel & 0xFF00;
            pos++;
        }

        bufferedImage.setRGB(0, 0, width, height, imagePixels, 0, width);

        Image image = Utils.bufferedImageToImage(bufferedImage);
        frameProcessorDto.setImage_depthMap(image);

        draw();

        labelToRect.clear();
        connectedComponents.clear();

        return frameProcessorDto;
    }

    private void init(VideoStream videoStream) {
        if (this.lastFrame != null) {
            this.lastFrame.release();
            this.lastFrame = null;
        }

        this.lastFrame = videoStream.readFrame();
        this.maxPixelValue = videoStream.getMaxPixelValue();

        this.width = lastFrame.getWidth();
        this.height = lastFrame.getHeight();

        if (histogram == null || histogram.length < maxPixelValue) {
            histogram = new float[maxPixelValue];
        }

        if (imagePixels == null || imagePixels.length < width * height) {
            imagePixels = new int[width * height];
        }

        if (depthPixels == null || depthPixels.length < width * height) {
            depthPixels = new int[width * height];
        }

        if (bufferedImage == null || bufferedImage.getWidth() != width || bufferedImage.getHeight() != height) {
            bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
        }
    }
}
