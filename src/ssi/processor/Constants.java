package ssi.processor;

public interface Constants {
    int GROUND_PIXEL_VAL = 6000;
    int V_HISTO_SCALE = 10;
    int U_HISTO_SCALE = 30;
    int MAX_DISTANCE = 4000;
}
