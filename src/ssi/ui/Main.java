package ssi.ui;

import org.opencv.core.*;
import org.openni.*;

import javafx.application.*;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.stage.*;

public class Main extends Application {
    public static void main(String[] args) {
        System.loadLibrary("OpenNI2");
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        OpenNI.initialize();

        launch();

        OpenNI.shutdown();
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            Pane root = FXMLLoader.load(getClass().getResource("window.fxml"));
            Scene scene = new Scene(root);
            primaryStage.setTitle("SSI3D");
            primaryStage.setResizable(false);
            primaryStage.setScene(scene);
            primaryStage.setOnCloseRequest(this::handleCloseApplication);
            primaryStage.show();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void handleCloseApplication(WindowEvent we) {
        System.exit(0);
    }
}
