package ssi.ui;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import org.openni.*;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ssi.collector.DataCollectorDto;
import ssi.collector.DataCollector;

public class Controller implements Initializable {
    @FXML
    private Pane pane_main;

    @FXML
    private ImageView imageView_depthMap;

    @FXML
    private ImageView imageView_segmentedImage;

    @FXML
    private ImageView imageView_vhist;

    @FXML
    private ImageView imageView_uhist;

    @FXML
    private ImageView imageView_ground;

    @FXML
    private Label lbl_distances;

    @FXML
    private Label lbl_deviceInfo;

    @FXML
    private Label lbl_file;

    @FXML
    private Label lbl_info;

    @FXML
    private Label lbl_vHistThreshold;

    @FXML
    private Label lbl_uHistThreshold;

    @FXML
    private Label lbl_minComponetSize;

    @FXML
    private Label lbl_erodeKernelSize;

    @FXML
    private Label lbl_dilateKernelSize;

    @FXML
    private TextField txt_recordFileName;

    @FXML
    private Button btn_startCamera;

    @FXML
    private Button btn_startRecording;

    @FXML
    private Button btn_openFile;

    @FXML
    private Slider slider_vHistThreshold;

    @FXML
    private Slider slider_uHistThreshold;

    @FXML
    private Slider slider_minComponetSize;

    @FXML
    private Slider slider_erodeKernelSize;

    @FXML
    private Slider slider_dilateKernelSize;

    @FXML
    private ChoiceBox<String> choiseBox_CameraOrFile;

    private ObjectProperty<String> distanceValuesProp;
    private ObjectProperty<String> deviceInfoValuesProp;

    private Recorder recorder;
    private DataCollector dataCollecter;

    @FXML
    protected void actionStartCamera() {
        if (btn_startCamera.getText().equals("Start")) {
            btn_startCamera.setText("Pause");
            dataCollecter.startStream();
            btn_openFile.setDisable(true);
            choiseBox_CameraOrFile.setDisable(true);
            btn_startRecording.setDisable(false);
        } else {
            btn_startCamera.setText("Start");
            dataCollecter.pauseStream();
            if (choiseBox_CameraOrFile.getValue().equals("File")) {
                btn_openFile.setDisable(false);
            }
            choiseBox_CameraOrFile.setDisable(false);
            btn_startRecording.setDisable(true);
        }
    }

    @FXML
    protected void actionStartRecording() {
        if (btn_startRecording.getText().equals("Record")) {
            if (txt_recordFileName.getText().isEmpty()) {
                lbl_info.setText("Error: No record file name!");
                lbl_info.setVisible(true);
                lbl_info.setTextFill(Color.RED);
            } else {
                recorder = Recorder.create("records/" + txt_recordFileName.getText() + ".oni");
                recorder.addStream(dataCollecter.getDeviceContainer().getDepthStream(), false);
                recorder.start();
                btn_startRecording.setText("Stop");
                lbl_info.setText("");
                lbl_info.setVisible(false);
                btn_startCamera.setDisable(true);
            }
        } else {
            recorder.stop();
            recorder.destroy();
            btn_startRecording.setText("Record");
            btn_startCamera.setDisable(false);
            txt_recordFileName.setText("");
        }
    }

    @FXML
    void actionOpenFile(ActionEvent event) {
        Stage stage = (Stage) pane_main.getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open oni file");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Oni files", "*.oni"));
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            dataCollecter.initStream(file.toString());
            lbl_file.setText(file.getName());
            lbl_file.setVisible(true);
            lbl_info.setText("");
            lbl_info.setVisible(false);
            btn_startCamera.setDisable(false);
        } else {
            lbl_info.setText("Error: Invalid file!");
            lbl_info.setVisible(true);
            lbl_info.setTextFill(Color.RED);
            btn_startCamera.setDisable(true);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        choiseBox_CameraOrFile.setItems(FXCollections.observableArrayList("Camera", "File"));
        choiseBox_CameraOrFile.setValue("File");

        choiseBox_CameraOrFile.setOnAction(this::handleChooseInput);

        btn_startCamera.setDisable(true);

        distanceValuesProp = new SimpleObjectProperty<>();
        lbl_distances.textProperty().bind(distanceValuesProp);

        deviceInfoValuesProp = new SimpleObjectProperty<>();
        lbl_deviceInfo.textProperty().bind(deviceInfoValuesProp);

        DataCollectorDto dataCollectorDto = new DataCollectorDto();
        dataCollectorDto.setImageView_depthMap(imageView_depthMap);
        dataCollectorDto.setImageView_segmentedImage(imageView_segmentedImage);
        dataCollectorDto.setImageView_ground(imageView_ground);
        dataCollectorDto.setImageView_uhist(imageView_uhist);
        dataCollectorDto.setImageView_vhist(imageView_vhist);
        dataCollectorDto.setDeviceInfoValuesProp(deviceInfoValuesProp);
        dataCollectorDto.setDistanceValuesProp(distanceValuesProp);
        dataCollectorDto.setSlider_vHistThreshold(slider_vHistThreshold);
        dataCollectorDto.setSlider_uHistThreshold(slider_uHistThreshold);
        dataCollectorDto.setSlider_minComponetSize(slider_minComponetSize);
        dataCollectorDto.setSlider_erodeKernelSize(slider_erodeKernelSize);
        dataCollectorDto.setSlider_dilateKernelSize(slider_dilateKernelSize);

        lbl_uHistThreshold.setText(String.format("%.0f", slider_uHistThreshold.getValue()));
        slider_uHistThreshold.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                lbl_uHistThreshold.setText(String.format("%.0f", slider_uHistThreshold.getValue()));
            }
        });

        lbl_vHistThreshold.setText(String.format("%.0f", slider_vHistThreshold.getValue()));
        slider_vHistThreshold.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                lbl_vHistThreshold.setText(String.format("%.0f", slider_vHistThreshold.getValue()));
            }
        });

        lbl_minComponetSize.setText(String.format("%.0f", slider_minComponetSize.getValue()));
        slider_minComponetSize.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                lbl_minComponetSize.setText(String.format("%.0f", slider_minComponetSize.getValue()));
            }
        });

        lbl_erodeKernelSize.setText(String.format("%.0f", slider_erodeKernelSize.getValue()));
        slider_erodeKernelSize.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                lbl_erodeKernelSize.setText(String.format("%.0f", slider_erodeKernelSize.getValue()));
            }
        });

        lbl_dilateKernelSize.setText(String.format("%.0f", slider_dilateKernelSize.getValue()));
        slider_dilateKernelSize.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                lbl_dilateKernelSize.setText(String.format("%.0f", slider_dilateKernelSize.getValue()));
            }
        });

        dataCollecter = new DataCollector(dataCollectorDto);
    }

    private void handleChooseInput(ActionEvent event) {
        if (choiseBox_CameraOrFile.getValue().equals("Camera")) {
            btn_openFile.setDisable(true);
            List<DeviceInfo> dI = OpenNI.enumerateDevices();
            if (!dI.isEmpty() && !dI.get(0).getName().contains("oni")) {
                dataCollecter.initStream(dI.get(0).getUri());
                lbl_info.setText("");
                lbl_info.setVisible(false);
                btn_startCamera.setDisable(false);
            } else {
                lbl_info.setText("Error: No structure sensor detected!");
                lbl_info.setVisible(true);
                lbl_info.setTextFill(Color.RED);
                btn_startCamera.setDisable(true);
            }
        } else {
            btn_openFile.setDisable(false);
            lbl_info.setText("");
            lbl_info.setVisible(false);
            if (!lbl_file.getText().isEmpty()) {
                btn_startCamera.setDisable(false);
            }
        }
    }
}
