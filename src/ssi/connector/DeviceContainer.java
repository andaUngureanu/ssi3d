package ssi.connector;

import java.util.List;

import org.openni.*;

public class DeviceContainer {
    private Device device;
    private VideoStream depthStream;

    public DeviceContainer(String url) {
        device = Device.open(url);
        depthStream = VideoStream.create(device, SensorType.DEPTH);

        setVideoMode();
    }

    private void setVideoMode() {
        List<VideoMode> supportedVideoModes = device.getSensorInfo(SensorType.DEPTH).getSupportedVideoModes();
        for (VideoMode videoMode : supportedVideoModes) {
            if (videoMode.getResolutionX() == 640 && videoMode.getResolutionY() == 480 && videoMode.getFps() == 30 && !device.isFile()) {
                depthStream.setVideoMode(videoMode);
                break;
            }
        }
    }

    public String printDeviceInfo() {
        VideoMode videoMode = depthStream.getVideoMode();
        PixelFormat pixelFormat = videoMode.getPixelFormat();

        StringBuilder deviceInfo = new StringBuilder();
        deviceInfo.append("Color Sensor: ").append(device.hasSensor(SensorType.COLOR)).append("\n");
        deviceInfo.append("Depth Sensor: ").append(device.hasSensor(SensorType.DEPTH)).append("\n");
        deviceInfo.append("IR Sensor: ").append(device.hasSensor(SensorType.IR)).append("\n");
        deviceInfo.append("Pixel format: ").append(pixelFormat.name()).append("\n");
        deviceInfo.append("Speed: ").append(videoMode.getFps()).append(" fps").append("\n");
        deviceInfo.append("Resolution: ").append(videoMode.getResolutionX()).append(" x ").append(videoMode.getResolutionY()).append("\n");
        deviceInfo.append("Max. pixel value: ").append(depthStream.getMaxPixelValue());
        return deviceInfo.toString();
    }

    public VideoStream getDepthStream() {
        return depthStream;
    }

    public Device getDevice() {
        return device;
    }
}
